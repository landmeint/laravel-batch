<?php

require_once 'Animal.php';

class Ape extends Animal
{
	public function get_legs()
	{
		$this->legs = 2;
		return $this->legs; 
	}

	public function yell()
	{
		echo "Yell : Auooo";
	}
}